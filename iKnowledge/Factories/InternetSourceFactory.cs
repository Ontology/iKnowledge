﻿using iKnowledge.Models.Business;
using LiteraturquellenController.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace iKnowledge.Factories
{
    public class InternetSourceFactory : NotifyPropertyChange
    {

        private Globals globals;

        public string NotifyChange_InternetSources
        {
            get { return nameof(InternetSources); }
        }

        private List<InternetSource> internetSources;
        public List<InternetSource> InternetSources
        {
            get { return internetSources; }
            private set
            {
                internetSources = value;
                RaisePropertyChanged(nameof(InternetSources));
            }
        }

        public async Task<dynamic> CreateInternetSourceList(InternetSourceResult internetSourceResult, List<clsObjectRel> tagsToTaggingSources, List<clsObjectRel> tagsToTag)
        {
            
            var literaturSourceToRef = internetSourceResult.RefToLiteraturSourceLeftRight.Select(refToList => new InternetSource
            {
                IdRef = refToList.ID_Object,
                NameRef = refToList.Name_Object,
                IdDirection = globals.Direction_LeftRight.GUID,
                IdRelationType = refToList.ID_RelationType,
                IdLiteratureSource = refToList.ID_Other,
                NameLiteratureSource = refToList.Name_Other
            }).ToList();

            literaturSourceToRef.AddRange(internetSourceResult.RefToLeteraturSourceRightLeft.Select(refToList => new InternetSource
            {
                IdRef = refToList.ID_Other,
                NameRef = refToList.Name_Other,
                IdDirection = globals.Direction_LeftRight.GUID,
                IdRelationType = refToList.ID_RelationType,
                IdLiteratureSource = refToList.ID_Object,
                NameLiteratureSource = refToList.Name_Object
            }));

            var result = (from literaturSource in literaturSourceToRef
                          join internetSource in internetSourceResult.InternetSourceToLiteraturSource on literaturSource.IdLiteratureSource equals internetSource.ID_Other
                          join url in internetSourceResult.UrlRels on internetSource.ID_Object equals url.ID_Object
                          join partner in internetSourceResult.CreatorRels on internetSource.ID_Object equals partner.ID_Object
                          select new InternetSource
                          {
                              IdRef = literaturSource.IdRef,
                              NameRef = literaturSource.NameRef,
                              IdDirection = literaturSource.IdDirection,
                              IdRelationType = literaturSource.IdRelationType,
                              IdLiteratureSource = literaturSource.IdLiteratureSource,
                              NameLiteratureSource = literaturSource.NameLiteratureSource,
                              IdUrl = url.ID_Other,
                              NameUrl = url.Name_Other,
                              IdPartner = partner.ID_Other,
                              NamePartner = partner.Name_Other,
                              OrderId = literaturSource.OrderId
                          }).OrderBy(inetSource => inetSource.OrderId).ThenBy(inetSource => inetSource.NameLiteratureSource).ToList();

            var tags = tagsToTag.GroupBy(tagItm => new { IdTag = tagItm.ID_Other, NameTag = tagItm.Name_Other }).Select(tagItm => new
            {
                IdTag = tagItm.Key.IdTag,
                NameTag = tagItm.Key.NameTag
            }).OrderBy(tag => tag.NameTag);

            var tagsToInternetSources = from tagToSource in tagsToTaggingSources
                                   join tag in tagsToTag on tagToSource.ID_Object equals tag.ID_Object
                                   join internetSourceItm in result on tagToSource.ID_Other equals internetSourceItm.IdLiteratureSource
                                   select new { IdTag = tag.ID_Other, IdLiteraturSource = internetSourceItm.IdLiteratureSource };

            var internetSourcesWithTag = result.Select(internetSource => new { internetSource, Tags = tagsToInternetSources.Where(tag => tag.IdLiteraturSource == internetSource.IdLiteratureSource).Select(tag => tag.IdTag) });

            return new { TagItems = tags, InternetSourceRefs = internetSourcesWithTag };
        }

        public InternetSourceFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}