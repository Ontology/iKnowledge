﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using PartnerModule;
using OntologyAppDBConnector;
using KnowledgeModule.Factories;
using KnowledgeModule.Services;
using iKnowledge.Models;
using LiteraturquellenController.Services;
using iKnowledge.Models.Business;
using iKnowledge.Factories;
using System.Net;

namespace iKnowledge.Controllers
{

    
    public class iKnowledgeController : Controller
    {
        
        private clsLogStates logStates = new clsLogStates();

        private ServiceAgentElastic knowledgeTreeService;
        private PartnerModule.Services.ServiceAgentElastic partnerService;
        private KnowledgeTreeFactory knowledgeTreeFactory;
        private PartnerModule.Factories.PartnerFactory partnerFactory;
        private List<PartnerModule.Models.RefToPartner> refToPartners;

        private LiteraturquellenController.Services.ElasticServiceAgent literatureService;

        private Globals globals;

        // GET: iKnowledge
        public ActionResult Index()
        {
            ViewBag.EndPoint = Guid.NewGuid().ToString();
            globals = new Globals(Server.MapPath("~/Content/Config_ont.xml"));
            var viewModel = new KnowledgeTreeViewModel();
            viewModel.UrlGetKnowledgeTree = "/iKnowledge/GetKnowledgeTree";
            viewModel.UrlUrlsPreview = "/iKnowledge/UrlsWithPreview";

            return View(viewModel);
            
        }

        public ActionResult KnowledgeTree()
        {
            var viewModel = new KnowledgeTreeViewModel();
            viewModel.UrlGetKnowledgeTree = "/iKnowledge/GetKnowledgeTree";
            viewModel.UrlUrlsPreview = "/iKnowledge/UrlsWithPreview";
            return View(viewModel);
        }

        public ActionResult UrlsWithPreview(string id)
        {
            ViewBag.RefId = id;
            return View();
        }

        public ActionResult GetDataUrlsWithPreview(string id)
        {

            var globals = new Globals(Server.MapPath("~/Content/Config_ont.xml"));
            literatureService = new ElasticServiceAgent(globals);
            var factory = new InternetSourceFactory(globals);
            clsOntologyItem refItem = null;
            if (string.IsNullOrEmpty(id))
            {
                refItem = new clsOntologyItem { GUID_Parent = "0e4ebffd02364420a9902702eb6618fa" };
            }
            else
            {
                refItem = literatureService.GetOItem(id, globals.Type_Object);
            }
            
            var serviceAgentTypedTagging = new TypedTaggingModule.Services.ServiceAgentElastic(globals);
            var sourcesTask = literatureService.GetInternetSourcesByRef(refItem);
            sourcesTask.Wait();
            var sources = sourcesTask.Result;

            var taggingSources = sources.RefToLiteraturSourceLeftRight.Select(litSource => new clsOntologyItem { GUID = litSource.ID_Other, Name = litSource.Name_Other, GUID_Parent = litSource.ID_Parent_Other, Type = litSource.Ontology }).ToList();
            taggingSources.AddRange(sources.RefToLeteraturSourceRightLeft.Select(litSource => new clsOntologyItem { GUID = litSource.ID_Object, Name = litSource.Name_Object, GUID_Parent = litSource.ID_Parent_Object, Type = globals.Type_Object }));
            var taggingSourcesGrouped = taggingSources.GroupBy(tagSrc => new { GUID = tagSrc.GUID, Name = tagSrc.Name, GUID_Parent = tagSrc.GUID_Parent, Type = tagSrc.Type }).Select(tagSrc => new clsOntologyItem
            {
                GUID = tagSrc.Key.GUID,
                Name = tagSrc.Key.Name,
                GUID_Parent = tagSrc.Key.GUID_Parent,
                Type = tagSrc.Key.Type
            }).ToList();

            var serviceResultTask = serviceAgentTypedTagging.GetTypedTagsByTaggingSources(taggingSourcesGrouped);
            serviceResultTask.Wait();
            var serviceResult = serviceResultTask.Result;

            var factoryTask = factory.CreateInternetSourceList(sources, serviceResult.TypedTagsToTaggingSources, serviceResult.TypedTagsToTags);
            factoryTask.Wait();

            var model = factoryTask.Result;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTags()
        {

            var globals = new Globals(Server.MapPath("~/Content/Config_ont.xml"));
            var serviceAgentLiteraturSource = new LiteraturquellenController.Services.ElasticServiceAgent(globals);
            

            var serviceAgentPartner = new PartnerModule.Services.ServiceAgentElastic(globals);
            var serviceAgentTypedTagging = new TypedTaggingModule.Services.ServiceAgentElastic(globals);

            var knowledgeItems = new clsOntologyItem { GUID_Parent = "0e4ebffd02364420a9902702eb6618fa" };
            var serviceResultLiteraturSourceTask = serviceAgentLiteraturSource.GetInternetSourcesByRef(knowledgeItems);
            serviceResultLiteraturSourceTask.Wait();
            var serviceResultLiteraturSource = serviceResultLiteraturSourceTask.Result;

            var taggingSources = serviceResultLiteraturSource.RefToLiteraturSourceLeftRight.Select(litSource => new clsOntologyItem { GUID = litSource.ID_Other, Name = litSource.Name_Other, GUID_Parent = litSource.ID_Parent_Other, Type = litSource.Ontology }).ToList();
            taggingSources.AddRange(serviceResultLiteraturSource.RefToLeteraturSourceRightLeft.Select(litSource => new  clsOntologyItem { GUID = litSource.ID_Object, Name = litSource.Name_Object, GUID_Parent = litSource.ID_Parent_Object, Type = globals.Type_Object}));

            var serviceResultPartnerTask = serviceAgentPartner.GetPartnersSimpleByRef(knowledgeItems);
            serviceResultPartnerTask.Wait();
            var serviceResultPartner = serviceResultPartnerTask.Result;

            taggingSources.AddRange(serviceResultPartner.PartnersOfRefLeftRight.Select(partner => new clsOntologyItem { GUID = partner.ID_Object, Name = partner.Name_Object, GUID_Parent = partner.ID_Parent_Object, Type = globals.Type_Object }));
            taggingSources.AddRange(serviceResultPartner.PartnersOfRefRightLeft.Select(partner => new clsOntologyItem { GUID = partner.ID_Other, Name = partner.Name_Other, GUID_Parent = partner.ID_Parent_Other, Type = globals.Type_Object }));

            var taggingSourcesGrouped = taggingSources.GroupBy(tagSrc => new { GUID = tagSrc.GUID, Name = tagSrc.Name, GUID_Parent = tagSrc.GUID_Parent, Type = tagSrc.Type }).Select(tagSrc => new clsOntologyItem
            {
                GUID = tagSrc.Key.GUID,
                Name = tagSrc.Key.Name,
                GUID_Parent = tagSrc.Key.GUID_Parent,
                Type = tagSrc.Key.Type
            }).ToList();

            var serviceResultTask = serviceAgentTypedTagging.GetTypedTagsByTaggingSources(taggingSourcesGrouped);
            serviceResultTask.Wait();
            var serviceResult = serviceResultTask.Result;

            if (serviceResult.Result.GUID == globals.LState_Error.GUID)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("DataError");
            }

            var typedTags = serviceResult.TypedTagsToTags.GroupBy(typedTag => new { GUID = typedTag.ID_Other, Name = typedTag.Name_Other, GUID_Parent = typedTag.ID_Parent_Other }).Select(typedTag => new clsOntologyItem
            {
                GUID = typedTag.Key.GUID,
                Name = typedTag.Key.Name,
                GUID_Parent = typedTag.Key.GUID_Parent,
                Type = globals.Type_Object
            }).OrderBy(typedTag => typedTag.Name).ToList();

            return Json(typedTags, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetKnowledgeTree()
        {
            var globals = new Globals(Server.MapPath("~/Content/Config_ont.xml"));
            knowledgeTreeService = new ServiceAgentElastic(globals);
            var knowledgeTreeTask = knowledgeTreeService.GetKnowledgeTree();
            knowledgeTreeTask.Wait();
            var result = knowledgeTreeTask.Result;

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("DataError");

            }

            knowledgeTreeFactory = new KnowledgeTreeFactory();
            var factoryResultTask = knowledgeTreeFactory.CreateKnowledgeTree(result.KnowledgeItems, result.KnowledgeItemsRel);
            factoryResultTask.Wait();
            var factoryResult = factoryResultTask.Result;

            if (factoryResult.Result.GUID == logStates.LogState_Error.GUID)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("DataError");
            }

            return Json(factoryResult.KnowledgeTree.Select(knowledgeTreeItem => knowledgeTreeItem.JsonDict).ToList(), JsonRequestBehavior.AllowGet);
        }


        public ActionResult PartnerTable(string id)
        {
            var globals = new Globals(Server.MapPath("~/Content/Config_ont.xml"));
            partnerService = new PartnerModule.Services.ServiceAgentElastic(globals);
            clsOntologyItem partner = null;
            if (string.IsNullOrEmpty(id))
            {
                partner = new clsOntologyItem { GUID_Parent = "0e4ebffd02364420a9902702eb6618fa" };
            }
            else
            {
                partner = partnerService.GetOItem(id, globals.Type_Object);
            }
            
            partnerFactory = new PartnerModule.Factories.PartnerFactory(globals);
            var serviceAgentTypedTagging = new TypedTaggingModule.Services.ServiceAgentElastic(globals);
            var partnerServiceTask = partnerService.GetPartnersByRef(partner);
            partnerServiceTask.Wait();
            var partnerServiceResult = partnerServiceTask.Result;

            if (partnerServiceResult.Result.GUID == globals.LState_Error.GUID)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("DataError");
            }

            var taggingSources = partnerServiceResult.PartnersOfRefLeftRight.Select(partnerItm => new clsOntologyItem { GUID = partnerItm.ID_Object, Name = partnerItm.Name_Object, GUID_Parent = partnerItm.ID_Parent_Object, Type = globals.Type_Object }).ToList();
            taggingSources.AddRange(partnerServiceResult.PartnersOfRefRightLeft.Select(partnerItm => new clsOntologyItem { GUID = partnerItm.ID_Other, Name = partnerItm.Name_Other, GUID_Parent = partnerItm.ID_Parent_Other, Type = globals.Type_Object }));

            var taggingSourcesGrouped = taggingSources.GroupBy(tagSrc => new { GUID = tagSrc.GUID, Name = tagSrc.Name, GUID_Parent = tagSrc.GUID_Parent, Type = tagSrc.Type }).Select(tagSrc => new clsOntologyItem
            {
                GUID = tagSrc.Key.GUID,
                Name = tagSrc.Key.Name,
                GUID_Parent = tagSrc.Key.GUID_Parent,
                Type = tagSrc.Key.Type
            }).ToList();

            var serviceResultTask = serviceAgentTypedTagging.GetTypedTagsByTaggingSources(taggingSourcesGrouped);
            serviceResultTask.Wait();
            var serviceResult = serviceResultTask.Result;

            if (serviceResult.Result.GUID == globals.LState_Error.GUID)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("DataError");
            }

            var typedTags = serviceResult.TypedTagsToTaggingSources.GroupBy(typedTag => new { GUID = typedTag.ID_Object, Name = typedTag.Name_Object, GUID_Parent = typedTag.ID_Parent_Object }).Select(typedTag => new clsOntologyItem
            {
                GUID = typedTag.Key.GUID,
                Name = typedTag.Key.Name,
                GUID_Parent = typedTag.Key.GUID_Parent,
                Type = globals.Type_Object
            }).OrderBy(typedTag => typedTag.Name).ToList();

            var partnerListTask = partnerFactory.CreatePartnerList(partnerServiceResult.PartnersOfRefLeftRight,
                        partnerServiceResult.PartnersOfRefRightLeft,
                        partnerServiceResult.PartnersToAddresses,
                        partnerServiceResult.Strassen,
                        partnerServiceResult.Postfaecher,
                        partnerServiceResult.Plzs,
                        partnerServiceResult.Orte,
                        partnerServiceResult.Lands,
                        partnerServiceResult.CommunicationToPartners,
                        partnerServiceResult.CommunicationToEmailAddresses,
                        partnerServiceResult.CommunicationToTels,
                        partnerServiceResult.CommunicationToFaxs,
                        partnerServiceResult.CommunicationToUrls, serviceResult.TypedTagsToTaggingSources, serviceResult.TypedTagsToTags);

            partnerListTask.Wait();
            var partnerListResult = partnerListTask.Result;

            if (partnerListResult == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("DataError");
            }

            return Json(partnerListResult, JsonRequestBehavior.AllowGet);
        }

        
    }
}