﻿var urlsDone = false;
var partnerDone = false;
var counts = [];

function filterReferencesByTags() {
    if (!urlsDone || !partnerDone) return;

    var tagChecks = $('.tagCheckLink').filter(':checked');

    $('.filterBadge').html('');

    if (tagChecks.length == 0) {
        $('.referenceItem').show();
    }
    else {
        $('.referenceItem').hide();
        var referenceRows = $('.referenceItem');
        var filterCount = 0;
        tagChecks.each(function () {
            var tag = $(this).attr('data-idtag');
            var selector = '[data-tagid*=_' + tag + '_]';
            var rowsWithTags = referenceRows.filter(selector);
            rowsWithTags.show();
            filterCount += rowsWithTags.length;
        });

        $('.filterBadge').html('gefiltert:' + filterCount);
    }
    
    $('.countLabel').html();
    var count = 0;
    for (var i in counts) {
        count += counts[i];
    }

    $('.countLabel').html(count);
}

function filterTagsByContent() {

    if (!urlsDone || !partnerDone) return;

    $('.tagCheckLink').closest('label').hide();
    var referenceRows = $('.referenceItem');

    referenceRows.each(function () {
        var tagIds = $(this).attr('data-tagid');
        if (tagIds != undefined) {
            var tagIdsSplit = tagIds.split("_");


            $(tagIdsSplit).each(function () {
                var tagId = this;
                if (tagId.length > 1) {
                    $('input[data-idtag="' + tagId + '"]').closest('label').show();
                }
            });
        }
        
        
        
    });

}


$(document).ready(function () {
    

    NProgress.start();

    jQuery.getJSON("/iKnowledge/iKnowledge/GetTags", function (data) {
        var tagsContainer = $('#tags');
        
        for (var ix in data) {
            var tag = data[ix];
            var label = $('<label style="margin-right:1px"></label>');
            var input = $('<input type="checkbox" class="tagCheckLink" name="tagCheckLink" data-idtag="' + tag.GUID + '"/>');
            var buttonDiv = $('<div class="checkbox btn">' + tag.Name + '</div>');

            label.append(input);
            label.append(buttonDiv)
            tagsContainer.append(label);
            

            $('.tagCheckLink').change(function () {
                filterReferencesByTags();
            });
        }
        
    });

    jQuery.getJSON("/iKnowledge/iKnowledge/GetKnowledgeTree", function (data) {
        console.log(data);
        $('#tree').treeview({
            data: data,
            onNodeSelected: function (event, data) {
                //$('#table').load("/iKnowledge/PartnerTable/1234")
                var id = data.href.replace('#_', '');
                if (data.nodeId == 0) {
                    id = "";
                }
                
                NProgress.start();
                urlsDone = false;
                partnerDone = false;

                $('.countLabel').html(0);
                $('.countLabel').html(0);

                var referenceContainer = $('#references');
                referenceContainer.html('');

                counts = [];

                $.getJSON('/iKnowledge/iKnowledge/PartnerTable/' + id, function (data) {

                    var tagButtons = $('#tagButtons');

                    counts.push(data.PartnerRefs.length);
                    
                    for (var jx in data.PartnerRefs) {


                        var refToPartner = data.PartnerRefs[jx];
                        var partner = refToPartner.partnerToRef.Partner;

                        var panelGroup = $('<div class="panel-group referenceItem"></div>');
                        if (refToPartner.Tags.length > 0) {
                            var tagId = '';
                            for (var kx in refToPartner.Tags) {
                                tagId += '_' + refToPartner.Tags[kx] + '_';

                            }
                            panelGroup.attr('data-tagid', tagId);
                        }

                        var panel = $('<div class="panel panel-info"></div>');
                        var panelHeading = $('<div class="panel-heading"></div>');
                        var panelTitle = $('<h4 class="panel-title">' + partner.Name + '</h4>');
                        var collapse = $('<div id="collapse' + jx + '"></div>')
                        var detail = $('<div class="panel-body"></div>')

                        var address = partner.Address;
                        if (address != undefined) {
                            var addressHtml = address.Name + '</br>';

                            if (address.NamePostfach != undefined) {
                                addressHtml += address.NamePostfach + '</br>';
                            }
                            else if (address.NameStrasse != undefined) {
                                addressHtml += address.NameStrasse + '</br>';
                            }

                            if (address.Plz != undefined) {
                                addressHtml += address.Plz;
                            }

                            if (address.NameOrt != undefined) {
                                addressHtml += ' ' + address.NameOrt;
                            }

                            addressHtml += '</br>';

                            if (address.NameLand != undefined) {
                                addressHtml += address.NameLand
                            }

                            var addressDiv = $('<div class="well well-sm"></div>');
                            var addressLabel = $('<span class="label label-default">Adresse</span></br></br>');
                            addressDiv.append(addressLabel);
                            addressDiv.append(addressHtml);

                            detail.append(addressDiv);
                        }

                        if (partner.EmailAddress.length > 0) {
                            var emailDiv = $('<div class="well well-sm"></div>');
                            var emailLabel = $('<span class="label label-default">Email-Adressen</span></br></br>');
                            emailDiv.append(emailLabel);
                            for (var ixEmail in partner.EmailAddress) {
                                var emailAddress = partner.EmailAddress[ixEmail];
                                var emailLink = $('<p><a href="mailto:' + emailAddress.Name + '">' + emailAddress.Name + '</a></p>');
                                emailDiv.append(emailLink);
                            }
                            detail.append(emailDiv);
                        }

                        if (partner.PhoneNumbers.length > 0) {
                            var phoneDiv = $('<div class="well well-sm"></div>');
                            var phoneLabel = $('<span class="label label-default">Telefonnummern</span></br></br>');
                            phoneDiv.append(phoneLabel);

                            for (var ixPhone in partner.PhoneNumbers) {
                                var phoneNumber = partner.PhoneNumbers[ixPhone];
                                var phoneLink = $('<p><a href="tel:' + phoneNumber.Name + '">' + phoneNumber.Name + '</a></p>');
                                phoneDiv.append(phoneLink);
                            }
                            detail.append(phoneDiv);
                        }

                        if (partner.FaxNumbers.length > 0) {
                            var phoneDiv = $('<div class="well well-sm"></div>');
                            var phoneLabel = $('<span class="label label-default">Faxnummern</span></br></br>');
                            phoneDiv.append(phoneLabel);

                            for (var ixPhone in partner.FaxNumbers) {
                                var phoneNumber = partner.FaxNumbers[ixPhone];
                                var phoneLink = $('<p><a href="tel:' + phoneNumber.Name + '">' + phoneNumber.Name + '</a></p>');
                                phoneDiv.append(phoneLink);
                            }
                            detail.append(phoneDiv);
                        }

                        if (partner.Urls.length > 0) {
                            var urlDiv = $('<div class="well well-sm"></div>');
                            var urlLabel = $('<p><span class="label label-default">Urls</span></br>');
                            urlDiv.append(urlLabel);
                            for (var ixUrl in partner.Urls) {
                                var url = partner.Urls[ixUrl];
                                var urlLink = $('<p><a href="' + url.NameUrl + '">' + url.NameUrl + '</a></p>');
                                urlDiv.append(urlLink);
                            }
                            detail.append(urlDiv);
                        }




                        panelGroup.append(panel);
                        panel.append(panelHeading);
                        panelHeading.append(panelTitle);

                        panel.append(collapse);
                        collapse.append(detail);
                        referenceContainer.append(panelGroup);


                    }


                    partnerDone = true;
                    if (urlsDone && partnerDone) {
                        NProgress.done();
                    }
                    filterReferencesByTags();
                    filterTagsByContent();
                });

                $.getJSON('/iKnowledge/iKnowledge/GetDataUrlsWithPreview/' + id, function (data) {

                    
                    var listGroup = $('<ul class="list-group"></ul>');
                    referenceContainer.append(listGroup);
                    var tagButtons = $('#tagButtonsLinks');

                    counts.push(data.InternetSourceRefs.length);
                    
                    for (var jx in data.InternetSourceRefs) {
                        
                        
                        var internetSourceWithTags = data.InternetSourceRefs[jx];
                        var internetSource = internetSourceWithTags.internetSource;

                        var listEntry = $('<li class="list-group-item referenceItem"></li>');
                        if (internetSourceWithTags.Tags.length > 0) {
                            var tagId = '';
                            for (var kx in internetSourceWithTags.Tags) {
                                tagId += '_' + internetSourceWithTags.Tags[kx] + '_';

                            }
                            listEntry.attr('data-tagid', tagId);
                        }
                        listGroup.append(listEntry);
                        var listEntryContent = $('<span>' + internetSource.NameLiteratureSource + '</span>');
                        var listEntryLink = $('<a href="' + internetSource.NameUrl + '" class="fullScreenBtn pull-right" target="_blank" role="button" title="Toggle fullscreen"><i class="glyphicon glyphicon-new-window"></i></a>');
                        listEntry.append(listEntryContent);
                        listEntry.append(listEntryLink);

                    }

                    
                    urlsDone = true;
                    if (urlsDone && partnerDone) {
                        NProgress.done();
                    }
                    filterReferencesByTags();
                    filterTagsByContent();
                });

                
            }
        });

        $('#tree').treeview('expandAll');
        NProgress.done();
    });


    $(".btn-panel-fullscreen").click(function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.children('i').hasClass('glyphicon-resize-full')) {
            $this.children('i').removeClass('glyphicon-resize-full');
            $this.children('i').addClass('glyphicon-resize-small');
        }
        else if ($this.children('i').hasClass('glyphicon-resize-small')) {
            $this.children('i').removeClass('glyphicon-resize-small');
            $this.children('i').addClass('glyphicon-resize-full');
        }
        $(this).closest('.panel').toggleClass('btn-fullscreen');
    });
});