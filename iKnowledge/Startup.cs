﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(iKnowledge.Startup))]
namespace iKnowledge
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
