﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iKnowledge.Models
{
    public class KnowledgeTreeViewModel
    {
        public string UrlGetKnowledgeTree { get; set; }
        public string UrlUrlsPreview { get; set; }
    }
}