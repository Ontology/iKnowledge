﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iKnowledge.Models.Business
{
    public class InternetSource
    {
        public string IdRef { get; set; }
        public string NameRef { get; set; }
        public string IdDirection { get; set; }
        public string IdRelationType { get; set; }
        public string IdLiteratureSource { get; set; }
        public string NameLiteratureSource { get; set; }
        public string IdInternetSource { get; set; }
        public string NameInternetSource { get; set; }
        public string IdUrl { get; set; }
        public string NameUrl { get; set; }
        public string IdPartner { get; set; }
        public string NamePartner { get; set; }
        public long OrderId { get; set; }
    }
}