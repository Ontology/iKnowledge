﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iKnowledge.Helpers
{
    public class CacheHelper
    {
        private static object locker = new object();
        
        private static clsLogStates logStates = new clsLogStates();
        public static clsOntologyItem SaveSimpleItemsToCache(string key, List<clsOntologyItem> objectList)
        {
            lock(locker)
            {
                var insertedObjectsCacheObj = HttpContext.Current.Cache[key];
                if (insertedObjectsCacheObj == null)
                {
                    HttpContext.Current.Cache[key] = objectList;
                }
                else
                {
                    var insertedObjects = (List<clsOntologyItem>)insertedObjectsCacheObj;
                    var objectsToInsert = (from objectToInsert in objectList
                                           join insertedObject in insertedObjects on objectToInsert.GUID equals insertedObject.GUID into objectsInserted
                                                 from insertedObject in objectsInserted.DefaultIfEmpty()
                                                 where insertedObject == null
                                                 select objectToInsert);

                    insertedObjects.AddRange(objectsToInsert);

                    HttpContext.Current.Cache[key] = insertedObjects;
                }
            }

            return logStates.LogState_Success.Clone();
        }

        public static List<clsOntologyItem> GetSimpleItemsFromCache(string key)
        { 
            lock(locker)
            {
                var inseredObjects = HttpContext.Current.Cache[key];

                if (inseredObjects == null)
                {
                    return new List<clsOntologyItem>();
                }
                else
                {
                    return (List<clsOntologyItem>) inseredObjects;
                }
            }
        }
    }
}